//= ../js/html5shiv.min.js
//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js
//= ../js/placeholders.min.js
//= ../../bower_components/device.js/lib/device.min.js
//= isotope.pkgd.min.js
//= ../../bower_components/jquery-form-styler/jquery.formstyler.min.js
//= ../../bower_components/jquery-validation/dist/jquery.validate.js

$(document).ready(function(){

    $(window).on('load', function(){
        var $grid = $('.grid').isotope({
            itemSelector: '.grid_item',
            masonry: {
                columnWidth: 280,
                gutter: 20
            }
        });
        var _m = 1;
        $('a.more_people').on('click', function(e){
            e.preventDefault();
            var self = $(this),
                _offset = $('.human:not(.my)').length,
                _total = parseInt(self.attr('data-total')),
                _not = self.attr('data-notsearch');
                $.ajax({
                    type: "POST",
                    url: "/items/load",
                    headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
                    data: {offset : _offset, nots : _not},
                    beforeSend: function(){
                      self.html('<i class="fa fa-refresh fa-spin"></i>').addClass('load');
                    },
                    success: function(data){
                        self.html('').removeClass('load');
                        if(_total <= (parseInt(_offset)+10)){
                            self.hide();
                        }
                        if(data == ''){
                            self.hide();
                        }
                        else{
                            var $items = $(data);
                            $grid.append($items).isotope('appended', $items).isotope( 'reloadItems' );
                        }

                    },
                    error: function(data){
                        console.log(data.responseText);
                    }
                });
            });
    });
    var _formState = false,
        _headerState = 'fix',
        _winScroll = $(window).scrollTop();
    if($('#search_people').length > 0) {
        $(window).on('scroll', function () {
            var _scroll = $(window).scrollTop(),
                _heading = $('#search_people').offset().top + $('#search_people').height(),
                _c = 0;
            _winScroll = $(window).scrollTop();
            if (_scroll > _heading) {
                $('body').addClass('scroll_header scroll_search');
                if (_c < 1) {
                    if (_formState) {
                        $('#search_form').show();
                    }
                    else {
                        $('#search_form').hide();
                    }
                    _c++;
                }
            }
            else {
                $('body').removeClass('scroll_header scroll_search');
                $('#search_form').show();
                _c = 0;
            }
        });
    }

    $('a.close_mess').on('click', function(e){
        e.preventDefault();
        $('.after_posting').fadeOut(200);
    });

    $('select').styler();

    $('header a.open_search').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        $('html, body').animate({scrollTop : 210}, 400, function(){
            $('#address').focus();
        });


    });
    $('header a.like_close').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        $('#search_form').fadeOut(200, function(){
            _formState = false;
            if($('html').hasClass('ios')) {
                _headerState = 'fix';
                $('body').removeClass('fix');
                $('header').css({
                    'position': 'fixed',
                    'top': 0
                });
                $('#search_form').css({
                    'position': 'fixed',
                    'top': '60px'
                });
            }
            self.fadeOut(200, function(){
                $('header a.open_search').fadeIn(200);
            });
        });
    });



    $('.hide_me').on('click', function(e){
        e.preventDefault();
        var _placeId = $(this).attr('data-id'),
            _user = $(this).attr('data-user');
        $.ajax({
            type: "PUT",
            url: "/user/hide",
            headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
            data: {place : _placeId, user : _user},
            success: function(data){
                location.reload();
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });
    $('.repeat').on('click', function(e){
        e.preventDefault();
        var _placeId = $(this).attr('data-id'),
            _user = $(this).attr('data-user');
        $.ajax({
            type: "PUT",
            url: "/user/repeat",
            headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
            data: {place : _placeId, user : _user},
            success: function(data){
                location.reload();
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });

    //
    $('input.social').on('change', function(){
       var _state = $(this).prop('checked');
        if(_state){
            $(this).next('label').hide();
            $(this).next('label').next('div').show();
        }
        else{
            $(this).next('label').show();
            $(this).next('label').next('div').hide();
        }
    });

    $(window).on('load', function(){
       $('input.social').each(function(){
           var _state = $(this).prop('checked');
           if(_state){
               $(this).next('label').hide();
               $(this).next('label').next('div').show();
           }
       });
        var _checkedProfiles = $('input.social:checked'),
            _addForm = $('#add_form');
        if(_checkedProfiles.length > 0){
            $('.step', _addForm).eq(1).removeClass('not_active');
        }
    });

    $('a.log').on('click', function(e){
       e.preventDefault();
       var _block = $('.social_wind');
       if(_block.css('display') == 'none'){
           var _right = ($(window).width() - $('.container').width())/2;
           _block.css({'right' : _right}).fadeIn(200);
       }
        else{
           _block.fadeOut(200);
       }
    });
    $(document).on('click', function(e){
        var _block = $('.social_wind'),
            _a = $('a.log');
        if( $(e.target).closest('a.log').length === 0 && $(e.target).closest(_block).length === 0){
            _block.fadeOut(200);
        }
    });

    $(document).on('click', '.links.not_active a', function(e){
       e.preventDefault();
        var _human = $(this).closest('.human');
        $('.already', _human).removeClass('hide');
        setTimeout(function(){
            $('.already', _human).addClass('hide');
        }, 3000);
    });
    $(document).on('click', '.human.out_radius a', function(e){
        e.preventDefault();
        var _human = $(this).closest('.human');
        $('.already', _human).removeClass('hide');
        setTimeout(function(){
            $('.already', _human).addClass('hide');
        }, 3000);
    });
    $(document).on('click', '.human.not_searched .links a', function(e){
        e.preventDefault();
        var _human = $(this).closest('.human');
        $('.already', _human).removeClass('hide');
        setTimeout(function(){
            $('.already', _human).addClass('hide');
        }, 3000);
    });

    $(document).on('click', '.links_for_admin a.delete', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id'), 
            ques = confirm('Вы уверены, что хотите удалить объявление?');
        if(ques){
            $.ajax({
                type: "DELETE",
                url: "/index/delete",
                headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
                data: {id : _id},
                success: function(){
                    location.reload();
                },
                error: function(data){
                    console.log(data);
                }
            });    
        }
        
    });

    $(document).on('click', '.links_for_admin a.delete', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id'),
            ques = confirm('Вы уверены, что хотите удалить объявление?');
        if(ques){
            $.ajax({
                type: "PUT",
                url: "/index/delete",
                headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
                data: {id : _id},
                success: function(){
                    location.reload();
                },
                error: function(data){
                    console.log(data.responseText);
                }
            });
        }

    });
    $(document).on('click', '.links_for_admin a.change', function(e){
        e.preventDefault();
        var _id = $(this).attr('data-id'),
            _state = $(this).attr('data-value'),
            ques = confirm('Вы уверены, что хотите скрыть/показать объявление?');
        if(ques){
            $.ajax({
                type: "PUT",
                url: "/index/change",
                headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
                data: {id : _id, state : _state},
                success: function(){
                    location.reload();
                },
                error: function(data){
                    console.log(data.responseText);
                }
            });
        }

    });

    if($('#add_form').length > 0) {

        $('#add_form').validate({
            rules:{
                type: {
                    required: true
                },
                period: {
                    required: true
                },
                price: {
                    required: true
                },
                address: {
                    required: true
                },
                time: {
                    required: true
                },
                comment: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: 'Это поле обязательно'
                },
                period: {
                    required: 'Это поле обязательно'
                },
                price: {
                    required: 'Это поле обязательно'
                },
                address: {
                    required: 'Это поле обязательно'
                },
                time: {
                    required: 'Это поле обязательно'
                },
                comment: {
                    required: 'Это поле обязательно'
                }
            }
        });
        $('#add_form').on('submit', function (e) {
            if(!$(this).valid()){
				e.preventDefault();
            }
        });
    }

});