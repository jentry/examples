<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Place;
use App\User;
use DB;


class IndexController extends Controller
{
	
    public function index()
    {
        $take = 10;
        if(Auth::guest()){
            $my = [];
            $first = Place::where([
                ['payed', 1]
            ])->take($take);
            $places = $first->orderBy('price', 'desc')->get();
        }
        else{
            $my = Place::where([['user_id', Auth::user()->id],['payed', 1]])->get();
            
            $first = Place::where([
                ['payed', 1],
                ['user_id', '!=', Auth::user()->id]
            ])->take($take-count($my));
            $places = $first->orderBy('price', 'desc')->get();
        }
        $counter = DB::table('options')->where('id', 1)->get();
		$owners = $this->owners($counter[0]->value, 'собственник', 'собственника', 'собственников');
        $total = Place::all()->count();
        return view('index.index', ['places' => $places, 'my' => $my, 'counter' => $counter, 'owners' => $owners, 'total' => $total]);
    }
	
	public function owners($n, $word1, $word2, $word3) {
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20){
			return $word3;
		}
		if ($n1 > 1 && $n1 < 5){
			return $word2;
		}
		if ($n1 == 1){
		return $word1;
		}
		return $word3;
	}

    public function ajaxload(Request $request)
    {
        if($request->ajax()){
            $take = 10;
            $offset = $request->offset;
            $not = $request->nots;
            if(Auth::guest()){
                $places = Place::where('payed', 1)->orderBy('price', 'desc')->skip($offset)->take($take)->get();
            }
            else{
                $places = Place::where([
                    ['user_id', '!=', Auth::user()->id],
                    ['payed', 1]
                ])->skip($offset)->take($take)->orderBy('price', 'desc')->get();
            }
            return view('ajax.items', ['places' => $places, 'not' => $not]);
        }

    }

    public function search(Request $request)
    {
        $flattype = $request->flattype;
        $lat = $request->lat;
        $lng = $request->lng;
        $minutes = 30;
        $radius = $minutes*0.5;

        $places1 = DB::table('places')->select(
            DB::raw("*,
                             ( 6371 * acos( cos( radians(".$lat.") ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(".$lng.")
                               ) + sin( radians(".$lat.") ) *
                               sin( radians( lat ) ) )
                             ) AS distance"))
            ->having("distance", "<", $radius)
            ->where([['flattype', $flattype], ['payed', 1]])
            ->orderBy("sortable", 'desc')
            ->get();
        $places2 = DB::table('places')->select(
            DB::raw("*,
                             ( 6371 * acos( cos( radians(".$lat.") ) *
                               cos( radians( lat ) )
                               * cos( radians( lng ) - radians(".$lng.")
                               ) + sin( radians(".$lat.") ) *
                               sin( radians( lat ) ) )
                             ) AS distance"))
            ->having("distance", ">", $radius)
            ->where([['flattype', $flattype], ['payed', 1]])
            ->orderBy("sortable", 'desc')
            ->get();
        $places = [$places1, $places2];
        $counter = DB::table('options')->where('id', 1)->get();
        return view('ajax.places', ['places' => $places, 'counter' => $counter]);
    }

    public function newsearch(Request $request)
    {
        $flattype = $request->flattype;
        $lat = $request->lat;
        $lng = $request->lng;
        $counter = DB::table('options')->where('id', 1)->get();

        $allPlaces = Place::where('flattype', $flattype)->orderBy("sortable", 'desc')->get();
        $places1 = [];
        $places2 = [];
        $places = [];

        foreach($allPlaces as $place){

            
            $distance = 6371 * (acos( cos( deg2rad($lat) ) * cos( deg2rad( $place->lat ) ) * cos( deg2rad( $place->lng ) - deg2rad($lng) ) + sin( deg2rad($lat) ) * sin( deg2rad( $place->lat ) ) ));
            $distance = round($distance, 3); //км
            
            $time = ($distance/0.85)*60;
            if((int)$place->time <= $time){
                array_push($places1, $place);
            }
            else{
                array_push($places2, $place);
            }
            
        }
        $places[0] = $places1;
        $places[1] = $places2;
        return view('ajax.places', ['places' => $places, 'counter' => $counter]);
    }

    public function yasearch(Request $request)
    {
        $flattype = $request->flattype;
        $allPlaces = Place::where('flattype', $flattype)->get();
        return $allPlaces;
    }

    public function yasearchitems(Request $request)
    {
        $yes = $request->yes;
        $no = $request->no;
        $flattype = $request->flattype;
        $counter = DB::table('options')->where('id', 1)->get();

        $places = [];
        $places1 = Place::whereIn('id', $yes)->where('payed', 1)->orderBy('sortable', 'desc')->get();
        $places2 = Place::whereIn('id', $no)->where('payed', 1)->orderBy('sortable', 'desc')->get();

        $places[0] = $places1;
        $places[1] = $places2;

        if(count($places[1]) == 0){
            $places[1] = Place::where([['flattype', $flattype],['payed', 1]])->get();
        }

        return view('ajax.places', ['places' => $places, 'counter' => $counter]);
    }

    public function about()
    {
        return view('index.about');
    }

    public function agreement()
    {
        return view('index.agreement');
    }

    public function oferta()
    {
        return view('index.oferta');
    }

    public function change(Request $request)
    {
        if($request->ajax()){
            $id = $request->id;
            $state = $request->state;
            $place = Place::find($id);
            $place->active = $state;
            $place->save();
        }
    }

    public function destroy(Request $request)
    {
        if($request->ajax()){
            $id = $request->id;
            $place = Place::find($id);
            $place->delete();
        }
    }
}
